package net.dgladkikh.autosiebel;

import net.dgladkikh.TestData.LeadForm;
import org.junit.Assert;
import org.testng.annotations.Test;
import org.openqa.selenium.By;

public class UsabilityLead extends TestBase {
    @Test

    public void UsabilityLead() throws Exception {
        appManager.getNavigationHelper().login(appManager.getNavigationHelper().getEnviroment(), test3);
        appManager.getNavigationHelper().goToLeadForm();

        LeadForm leadForm = new LeadForm();
        leadForm.setFullName("Ghjkdsfj");
        leadForm.setContactName("jsdfglkj");
        leadForm.setEmail("kdljf@jdfiu.ru");

        appManager.getPageHelper().fillLeadForm(leadForm);
        appManager.getNavigationHelper().finishLeadCeation();
        appManager.getNavigationHelper().toLeadList();
        String leadName = appManager.getNavigationHelper().getAttribute(By.id("1_s_2_l_Full_Company_Name__rus_"));

        Assert.assertEquals(leadName, leadForm.getFullName());
    }




}
