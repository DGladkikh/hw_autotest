package net.dgladkikh.autosiebel;

import net.dgladkikh.TestData.User;
import net.dgladkikh.TestData.UserBuilder;
import net.dgladkikh.helpers.AppManager;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;



public class TestBase {
protected final AppManager appManager = new AppManager();
    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        appManager.init();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        appManager.stop();
    }


    User test3 = new UserBuilder().setLogin("test3").setPass("test3").createUser();
    User abs = new UserBuilder().setLogin("123").setPass("123").createUser();
}
