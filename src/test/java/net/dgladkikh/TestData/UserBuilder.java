package net.dgladkikh.TestData;

public class UserBuilder {
    private String login;
    private String pass;

    public UserBuilder setLogin(String login) {
        this.login = login;
        return this;
    }

    public UserBuilder setPass(String pass) {
        this.pass = pass;
        return this;
    }

    public User createUser() {
        return new User(login, pass);
    }
}