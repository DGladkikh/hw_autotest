package net.dgladkikh.TestData;

public class LeadForm {
    private String fullName;
    private String contactName;
    private String email;
    private int inn;
    private int kpp;

    public LeadForm(){
        this.inn= (int)((Math.random()*1000000)+1000000);
        this.kpp= (int)((Math.random()*1000000)+1000000);
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getInn() {
        return inn;
    }

    public void setInn(int inn) {
        this.inn = inn;
    }

    public int getKpp() {
        return kpp;
    }

    public void setKpp(int kpp) {
        this.kpp = kpp;
    }



    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
