package net.dgladkikh.helpers;

import net.dgladkikh.helpers.NavigationHelper;
import net.dgladkikh.helpers.PageHelper;
import net.dgladkikh.helpers.ProjectHelper;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import static org.testng.Assert.fail;

public class AppManager {

    private PageHelper pageHelper;
    protected WebDriver driver;
    private ProjectHelper projectHelper;
    private NavigationHelper navigationHelper;
    protected StringBuffer verificationErrors;
    public Properties properties;
    public String enviroment;





    public void init() throws IOException {
        System.setProperty("webdriver.chrome.driver", "libs\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        verificationErrors = new StringBuffer();

        projectHelper = new ProjectHelper(driver, verificationErrors);
        navigationHelper  = new NavigationHelper(driver);
        pageHelper = new PageHelper(driver);

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }




    public void stop() {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }


    public NavigationHelper getNavigationHelper() { return navigationHelper; }

    public ProjectHelper getProjectHelper() {
        return projectHelper;
    }

    public PageHelper getPageHelper() {
        return pageHelper;
    }


}
